let Sequelize = require('sequelize');

let sequelize = new Sequelize('oficinaNode', 'postgres', 'r1o1d1o1', {
    host:'10.10.19.132',
    port:65310,
    dialect: 'postgres',
    pool:{
        max:5,
        min:0,
        acquire:3000,
        idle:10000
    },
    operatorsAliases: false
});

module.exports = sequelize;