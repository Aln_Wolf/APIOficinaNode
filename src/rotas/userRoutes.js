let Router = require('restify-router').Router;
let usuarios = require('../middlewares/midUser');
let router = new Router();

router.post('/', usuarios.inserir);
router.get('/listar', usuarios.listar);
router.post('/deletar', usuarios.deletar);
router.put('/edit/:id', usuarios.editar);

module.exports = router;