//chama o arquivo usuarios para que os métodos dele possam ser usados neste arquivo
let user = require('../controllers/usuarios');

/*
    Método que pega parametros do body da chamada da api e envia para fazer a query de INSERT no banco de dados
*/
let midInserir = (req,res) => {
    let email = req.body.email;
    let nome = req.body.nome;
    //Chama método inserir do controller
    user.inserir({email, nome}, (result) => {
        res.send(result);
    })
}


/*
    Método para chamar a lista dos dados do banco de dados
*/
let midListar = (req, res) => {
    //Chama método listar do controller
    user.listar((result) => {
        res.send(result);
    })
}

/*
    Método que pega o nome por parâmetro do body da chamada da API e envia para fazer a query de DELETE no Banco de dados
*/
let midDeletar = (req, res) => {
    let nome = req.body.nome;
    //Chama método deletar do controller
    user.deletar({nome}, (result) => {
        res.send(result);
    })
}

/*
    Método que pega o parâmetro id da URL, e pega email e nome por parâmetro do body da chamada da API e envia para fazer a query UPDATE no Banco de dados
*/
let midEditar = (req, res) => {
    let after = req.params.id;
    let updated = {
        email: req.body.email,
        nome: req.body.nome
    }
    //Chama método editar do controller
    user.editar(after, updated, (result) => {
        res.send(result);
    })

}


/*
Exporta os métodos deste arquivo para poderem ser chamados em outros arquivos
*/
module.exports.inserir = midInserir;
module.exports.listar = midListar;
module.exports.deletar = midDeletar;
module.exports.editar = midEditar;