let sequelize = require('../config/sequelize');

//Criação do Crud para usuario

let inserir = (user, callback) => {
    sequelize.query(`SELECT * FROM pessoa WHERE email="${user.email}" AND nome="${user.nome}"`)
            .spread((result, metadata) => {
                if(result.length == 0){
                    sequelize.query(`INSERT INTO pessoa (email, nome) VALUES ("${user.email}", "${user.nome}")`)
                        .spread((results, metadados) => {
                            callback({id:results});
                        })
                }else{
                    callback('O usuario já existe');
                }
            })
}


let listar = (callback) => {
    sequelize.query(`SELECT * from pessoa`)
        .spread((result, metadata) => {
            callback(result);
        })
}


let deletar = (user, callback) => {
    sequelize.query(`SELECT * FROM pessoa WHERE nome="${user.nome}"`)
            .spread((result, metadata) => {
                if(result.length == 0){
                    callback('Não é possivel deletar este usuário, usuario não existente!');
                }else{
                    sequelize.query(`DELETE FROM pessoa WHERE nome="${user.nome}"`)
                    .spread((results, metadatas) => {
                        sequelize.query('SELECT * from pessoa')
                            .spread((resultado, metadados) => {
                                callback({
                                    message: `Usuário ${user.nome} deletado com sucesso`,
                                    table_atual: resultado
                                    });
                            })
                    })
                }
            })
}


let editar = (after, next, callback) => {
    sequelize.query(`UPDATE pessoa SET email="${next.email}", nome="${next.nome}" WHERE id="${after}"`)
        .spread((result, metadata) => {
            callback(result);
        })
}


module.exports.inserir = inserir;
module.exports.listar = listar;
module.exports.deletar = deletar;
module.exports.editar = editar;