let restify = require('restify');
let bodyParser = require('body-parser');
let cors = require('cors');
let sequelize = require('./src/config/sequelize');
let router = require('./src/rotas/userRoutes');


let port = 3000;


let server = restify.createServer({name: "MyAPI"});

server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());
server.use(cors());


//rotas da API
router.applyRoutes(server);


sequelize.authenticate()
    .then(() => {
        console.log('Banco de dados conectado com sucesso');
    })
    .catch((error) => {
        console.log(`Não foi possivel conectar ao banco de dados por causa do erro: ${error}`);
    })


server.listen(port, () => {
    console.log(`Servidor conectado na porta ${port}`);
})